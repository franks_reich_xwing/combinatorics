"""Calculate all possible outcomes and probabilities for a
dice roll."""
import itertools
import functools
import operator
from enum import Enum, auto


class AttackDiceSides(Enum):
    """X-Wing Attack Dice Sides."""
    FOCUS = auto()
    HIT = auto()
    CRIT = auto()
    BLANK = auto()


dice_side_probability = {
    AttackDiceSides.FOCUS: 2.0 / 8.0,
    AttackDiceSides.HIT: 3.0 / 8.0,
    AttackDiceSides.CRIT: 1.0 / 8.0,
    AttackDiceSides.BLANK: 2.0 / 8.0
}


dice_sides = [AttackDiceSides.FOCUS,
              AttackDiceSides.HIT,
              AttackDiceSides.CRIT,
              AttackDiceSides.BLANK]


def calculate_roll_probability(combination):
    """Takes a combination from the initial roll and calculates the probability
    for this combination based on the X-Wing attack dice probabilities.
    """
    probabilities = list(map(lambda side: dice_side_probability[side],
                             combination))
    return (combination, functools.reduce(operator.mul, probabilities))


def apply_target_lock(combinations, reroll_sides):
    """Applies the target lock to all combinations."""
    result = map(lambda x: apply_target_lock_to_combination(x, reroll_sides),
                 combinations)
    return itertools.chain(*result)


def apply_target_lock_to_combination(combination, reroll_sides):
    """Applies the target lock reroll to the combination. Due to the random
    nature of the target lock, the function returns multiple results with
    probabilities per combination.
    """
    number_of_dice_to_reroll = _count_dice_sides(combination[0], reroll_sides)
    if number_of_dice_to_reroll > 0:
        remaining_dice = list(filter(lambda side: side not in reroll_sides, combination[0]))
        reroll_results = list(itertools.product(dice_sides, repeat=number_of_dice_to_reroll))
        reroll_results = list(map(calculate_roll_probability, reroll_results))
        return list(map(
            lambda reroll_result: (remaining_dice + list(reroll_result[0]),
                                   combination[1] * reroll_result[1]),
            reroll_results))
    else:
        return [combination]


def apply_focus(combinations):
    """Changes all focus results into hits."""
    return list(map(apply_focus_to_combination, combinations))


def apply_focus_to_combination(combination):
    """Change all focus results to hit results."""
    new_result = ([AttackDiceSides.HIT if side == AttackDiceSides.FOCUS
                   else side for side in combination[0]], combination[1])
    return new_result


def apply_rac_ability(combinations):
    """Changes a focus to a crit in each combination."""
    return list(map(apply_rac_ability_to_combination, combinations))


def apply_rac_ability_to_combination(combination):
    """Changes a focus to a crit."""
    combination, probability = combination
    if AttackDiceSides.FOCUS in combination:
        index = combination.index(AttackDiceSides.FOCUS)
        combination[index] = AttackDiceSides.CRIT
        return (combination, probability)
    else:
        return (combination, probability)


def _count_dice_sides(dice, sides_to_count):
    count = 0
    for die in dice:
        if die in sides_to_count:
            count += 1
    return count


def create_distribution_for_success(combinations, success):
    """Calculates the distribution for success."""
    distribution = {}
    for combination in combinations:
        number_of_success = _count_dice_sides(combination[0], success)
        if number_of_success in distribution:
            distribution[number_of_success] += combination[1]
        else:
            distribution[number_of_success] = combination[1]
    return distribution


NUMBER_OF_DICE = 3
combinations = itertools.product(dice_sides, repeat=NUMBER_OF_DICE)
combinations = list(map(list, combinations))
combinations = map(calculate_roll_probability, combinations)
combinations = apply_target_lock(combinations, [AttackDiceSides.BLANK])
combinations = apply_rac_ability(combinations)
combinations = apply_focus(combinations)
distribution = create_distribution_for_success(
    combinations,
    [AttackDiceSides.CRIT, AttackDiceSides.HIT])


print(distribution)
